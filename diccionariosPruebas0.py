#diccionariosPruebas0.py
import os

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('clear')

def conteoLetras():
    frase = 'Hoy a salido el sol y es una mañana estupenda'
    conteo = {}

    for letra in frase.lower():
        if letra not in conteo:
            conteo[letra] = 1
        else:
            conteo[letra] +=1

    for k, v in conteo.items():
        print(k,v)
    
    continuar()

def stock():
    deposito01 = {'manzanas':100,'naranjas':3}
    menu = True
    while menu:
        print('Stock de productos')
        print('1. consultar stock de un producto')
        print('2. idem 1 pero si no está lo agrega con cantidad 0')
        print('3. listar')
        print('4. agregar o quitar cantidad')
        print('9. salir')
        opcion = ''
        while (opcion not in ('1','2','3','4','9')):
            opcion = input('-> ')

        if (opcion == '1'):
            producto = input('producto?: ')
            dato = deposito01.get(producto,'No existe el producto')
            print(producto,dato)

        elif (opcion == '2'):
            producto = input('producto?: ')
            dato = deposito01.setdefault(producto,0)
            print(producto,dato) 

        elif (opcion == '3'):   
            for legajo,datos in deposito01.items():
                print(legajo,datos)   

        elif (opcion == '9'):
            menu = False
        continuar()

#principal
os.system('clear')
conteoLetras()
#stock()
